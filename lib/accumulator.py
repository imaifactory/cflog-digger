#!/usr/bin/python

import boto
import boto.dynamodb2 
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import StringIO
import gzip
import random

class Accumulator:

    def __init__(
        self,
        batch_id,
        b_in_name,b_in_ak,b_in_sk, b_in_prefix,
        b_out_name,b_out_ak,b_out_sk, b_out_prefix,
        ddb_region,ddb_table,ddb_ak,ddb_sk
    ):

        self.batch_id = batch_id

        self.s3_in    = S3Connection(b_in_ak, b_in_sk)
        self.b_in  = self.s3_in.get_bucket(b_in_name)
        self.b_in_prefix = b_in_prefix

        self.s3_out    = S3Connection(b_out_ak, b_out_sk)
        self.b_out = self.s3_out.get_bucket(b_out_name)
        self.b_out_prefix = b_out_prefix

        self.ddb = boto.dynamodb2.connect_to_region(
            ddb_region, 
            aws_access_key_id=ddb_ak, 
            aws_secret_access_key=ddb_sk
        )

        self.ddb_table = ddb_table

        self.hash_table = dict()

    def init_hash_table(self):
        self.hash_table = dict()

    def add_to_hash_table(self,line):
        data = line.split('\t')
        key = '{} {}:00 {}'
        key = key.format(data[0],data[1][:5],data[2])

        if key in self.hash_table:
            return self._append_to_item(key,data)
        else:
            return self._init_item(key,data)

    def _init_item(self,key,data):
        item = {
            'rc'   : 1,
            'b'    : long(data[3]),
            'hit'  : 1 if data[-5] == 'Hit' else 0,
            'miss' : 1 if data[-5] == 'Miss' else 0,
            'other': 1 if data[-5] not in ('Hit', 'Miss') else 0
        }
        self.hash_table[key] = item
        return True

    def _append_to_item(self,key,data):
        item = self.hash_table[key]
        item['b']  = item['b'] + long(data[3])
        item['rc'] = item['rc'] + 1
        if data[-5] == 'Hit':
            item['hit'] = item['hit'] + 1
        elif data[-5] == 'Miss':
            item['miss'] = item['miss'] + 1
        else:
            item['other'] = item['other'] + 1
        self.hash_table[key] = item
        return True

    def commit_to_s3(self,seqNo,objsToProcess):

        ns = StringIO.StringIO()
        fp = gzip.GzipFile(self.batch_id + ".txt.gz",fileobj=ns,mode="wb")

        processed = []
        failed = []
        for K in objsToProcess:
            compressedFile = StringIO.StringIO()
            try:
                K.get_contents_to_file(compressedFile)
                processed.append(K.name)
            except Exception:
                failed.append(K.name)

            compressedFile.seek(0)
            decompressedFile = gzip.GzipFile(fileobj=compressedFile, mode="rb")
        
            for l in decompressedFile:
                if not l.startswith("#"):
                    self.add_to_hash_table(l)

            decompressedFile.close()

        for k, r in self.hash_table.iteritems():
            line = '{}\t{}\t{}\t{}\t{}\t{}\t{}\n'
            line = line.format(k[:18],k[19:],r['rc'],r['b'],r['hit'],r['miss'],r['other'])
            fp.write(line)
        fp.close()

        output_filename = self.batch_id + "-" + str(seqNo) + ".txt.gz"
        output_key = Key(self.b_out,self.b_out_prefix + output_filename)
        ns.seek(0)
        output_file = StringIO.StringIO(ns.getvalue())

        try:
            output_key.set_contents_from_file(output_file)

        except Exception:
            processed = []
            for K in objsToProcess:
                failed.append(K.name)

        output_file.close()

        return ( output_filename, processed, failed )
    
    def remove_commited(self,filename):
        self.b_out.delete_key(filename)
    
    def list_object(self):
        list = self.b_in.list(prefix=self.b_in_prefix)
        result = []
        for object in list:
            result.append(object)
        random.shuffle(result)
        return result

    def lock_key(self,keyname):
        try:
            self.ddb.put_item(
                self.ddb_table,
                { "s3key" : { "S" : keyname } },
                expected = {
                    "s3key" : { "Exists" : False }
                }
            )
            return True
        except:
            return False

    def release_key(self,keyname):
        try:
            self.ddb.delete_item(
                self.ddb_table,
                { "s3key" : { "S" : keyname } }
            )
            return True

        except Exception:
            return False



