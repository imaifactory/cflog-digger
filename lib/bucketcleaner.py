#!/usr/bin/python

import boto
import boto.dynamodb2 
from boto.dynamodb2.table import Table
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import random

class BucketCleaner:

    def __init__(
        self,
        batch_id,
        b_name,b_ak,b_sk,
        b_inprefix,b_outprefix,
        ddb_region,
        ddb_src_table,
        ddb_lock_table,
        ddb_ak,ddb_sk
    ):

        self.batch_id = batch_id

        self.s3           = S3Connection(b_ak, b_sk)
        self.bucket       = self.s3.get_bucket(b_name)
        self.s3_inprefix  = b_inprefix
        self.s3_outprefix = b_outprefix

        self.ddb_conn = boto.dynamodb2.connect_to_region(
            ddb_region, 
            aws_access_key_id=ddb_ak, 
            aws_secret_access_key=ddb_sk
        )

        self.ddb_src_table_name  = ddb_src_table
        self.ddb_lock_table_name = ddb_lock_table

        self.ddb_src_table = Table(ddb_src_table,connection=self.ddb_conn)
        self.ddb_lock_table = Table(ddb_lock_table,connection=self.ddb_conn)

    def list_s3keys(self):
        list = self.bucket.list(prefix=self.s3_inprefix)
        result = []
        for object in list:
            result.append(object)
        random.shuffle(result)
        return result

    def copy(self,src_key):
        try:
            data = src_key.get_contents_as_string()
            dst_key = Key(
                bucket=self.bucket, 
                name=self.s3_outprefix + src_key.name
            )
            dst_key.set_contents_from_string(data)
        except Exception:
            return False
        else:
            return True

    def cleanup(self,key):

        try:
            self.bucket.delete_key(key.name)
        except:
            return False
        else:
            return True

    def check_key_avairability(self,keyname):
        if self.ddb_conn.get_item(
            self.ddb_src_table_name,
            { "s3key" : { "S" : keyname } },
            consistent_read=True 
        ):
            return True
        else:
            return False
            
    def lock_key(self,keyname):
        try:
            self.ddb_conn.put_item(
                self.ddb_lock_table_name,
                { "s3key" : { "S" : keyname } },
                expected = {
                    "s3key" : { "Exists" : False }
                }
            )
            return True
        except:
            return False

    def release_key(self,keyname):
        try:
            self.ddb_conn.delete_item(
                self.ddb_lock_table_name,
                { "s3key" : { "S" : keyname } }
            )
            return True

        except Exception:
            return False



