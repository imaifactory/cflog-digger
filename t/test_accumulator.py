import unittest

import boto
import boto.dynamodb2 
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from pprint import pprint

from accumulator import Accumulator

class testAccumulator(unittest.TestCase):

    ak = 'AKIAIZTZKWHIKC7EPNEA'
    sk = 'u/VCRtnGrjsBREtjyhPochxq71WxvaHvR9hgxcLp'
    s3bucket = 'imai-factory-test'
    ddb = 'imai-factory-test'

    def create_accumulator(self):
        return Accumulator(
            'sample_batch_id',
            self.s3bucket, self.ak, self.sk, 'cflog/',
            self.ddb, self.ak, self.sk, 'cflog_out/',
            'ap-northeast-1', 'imai-factory-test', self.ak, self.sk 
        )

    def get_s3bucket(self):
        conn = S3Connection(self.ak, self.sk)
        return conn.get_bucket(self.s3bucket)

    def test_01_list_object(self):
        accumulator = self.create_accumulator()
        objs1  = accumulator.list_object()
        len1  = sum(1 for _ in objs1)

        s3 = self.get_s3bucket()
        objs2 = s3.list(prefix='cflog/')
        len2 = sum(1 for _ in objs2)

        self.assertEqual(len1,len2)
        
    def test_02_move_processed_object(self):
        self.assertTrue(self.create_accumulator().move_processed_object('test'))

    def test_03_format_datetime(self):
        obj = self.create_accumulator()


if __name__ == '__main__':
    unittest.main()
