#!/usr/bin/env python

import random
import string
from datetime import datetime
import logging
import sys
import json

from accumulator import Accumulator

##functions##
def commit_to_s3(accumulator,seqNo,objsToProcess):
    try:
        (filename, processed, failed) = accumulator.commit_to_s3(seqNo,objsToProcess)
    except:
        log.error("rollback seqNo: %s", seqNo)
        for i in objsToProcess:
            accumulator.release_key(i.name)
        return False

    for j in processed:
        log.info('Commited %s in %s', j, filename)
    for k in failed:
        if not accumulator.release_key(k):
            log.error('Faild to process and release the lock: %s',k, exc_info=True)
        else:
            log.warn("Failed to process: %s", k, exc_info=True)


##main##
try:
    config_path = sys.argv[1]
    config_file = open(config_path)
    config = json.load(config_file)

    loop = config['loop']

    k_ak = config['s3_bucket_in_ak']
    k_sk = config['s3_bucket_in_sk']

    k_ak_out = config['s3_bucket_out_ak']
    k_sk_out = config['s3_bucket_out_sk']

    bn = config['s3_bucket_in_name']
    bn_logprefix = config['s3_bucket_in_logprefix']
    bn_out = config['s3_bucket_out_name']
    bn_out_logprefix = config['s3_bucket_out_logprefix']

    dynamodb_region = config['dynamodb_region']
    dynamodb_ak = config['dynamodb_ak']
    dynamodb_sk = config['dynamodb_sk']
    dynamodb_table = config['dynamodb_table']

except Exception:
    sys.exit(["Failed to load config from:%s" % config_path])

try:
    bn_logprefix = sys.argv[2]
except:
    pass

batchId = datetime.now().strftime("%Y%m%d%H%M%S") + "_" + "".join(random.choice(string.ascii_uppercase) for x in range(12))
FORMAT = "%(asctime)s %(message)s"
#logging.basicConfig(format=FORMAT,filename="enload_" + batchId + ".log", level=logging.INFO)
logging.basicConfig(format=FORMAT,level=logging.INFO)
log = logging.getLogger("ex")


try:
    accumulator = Accumulator(
        batchId,
        bn, k_ak, k_sk, bn_logprefix,
        bn_out, k_ak_out, k_sk_out, bn_out_logprefix,
        dynamodb_region, dynamodb_table, dynamodb_ak, dynamodb_sk
    )

except Exception as e:
  log.error("Failed to init Accumulator instance", exc_info=True)
  sys.exit()

try:
    log.info("Starting: %s",batchId)

    objs = accumulator.list_object()
    objsToProcess = []
    i = 0
    seqNo = 0
    for K in objs:
        if accumulator.lock_key(K.name):
            objsToProcess.append(K)
            if i >= loop:
                commit_to_s3(accumulator,seqNo,objsToProcess)
                accumulator.init_hash_table()
                objsToProcess = []
                seqNo+=1
                i = 0
            else:
                i+=1

    commit_to_s3(accumulator,seqNo,objsToProcess)
    accumulator.init_hash_table()
    log.info("Finished: %s",batchId)

except Exception as e:
  log.error("Failed to Copy Data", exc_info=True)
  sys.exit()
