#!/usr/bin/env python

import random
import string
from datetime import datetime
import logging
import sys
import json

from bucketcleaner import BucketCleaner

##main##
try:
    config_path = sys.argv[1]
    config_file = open(config_path)
    config = json.load(config_file)

    k_ak = config['s3_bucket_in_ak']
    k_sk = config['s3_bucket_in_sk']
    bn = config['s3_bucket_in_name']
    bn_inprefix = config['s3_bucket_in_logprefix']
    bn_outprefix = config['s3_bucket_in_logprefix_for_cleanup']

    dynamodb_region = config['dynamodb_region']
    dynamodb_ak = config['dynamodb_ak']
    dynamodb_sk = config['dynamodb_sk']
    dynamodb_src_table = config['dynamodb_table']
    dynamodb_lock_table = config['dynamodb_table_for_cleanup']

except Exception:
    sys.exit(["Failed to load config from:%s" % config_path])


batchId = datetime.now().strftime("%Y%m%d%H%M%S") + "_" + "".join(random.choice(string.ascii_uppercase) for x in range(12))
FORMAT = "%(asctime)s %(message)s"
logging.basicConfig(format=FORMAT,level=logging.INFO)
log = logging.getLogger("ex")

try:
    cleaner = BucketCleaner(
        batchId,
        bn, k_ak, k_sk,
        bn_inprefix,bn_outprefix,
        dynamodb_region, 
        dynamodb_src_table, 
        dynamodb_lock_table,
        dynamodb_ak, dynamodb_sk
    )
except Exception as e:
  log.error("Failed to init BucketCleaner instance", exc_info=True)


try:
    log.info("Starting: %s",batchId)

    s3keys = cleaner.list_s3keys()
    for s3key in s3keys:

        if not cleaner.check_key_avairability(s3key.name):
            log.info("Skipping: %s", s3key.name)
            continue

        if not cleaner.lock_key(s3key.name):
            log.info("Failed to lock: %s", s3key.name)
            continue

        if cleaner.copy(s3key):
            if not cleaner.cleanup(s3key):
                cleaner.release_key(s3key)
                log.warn("Failed to delete old file: %s", s3key.name)
            else:
                log.info("Cleanup: %s", s3key.name)
        else:
            cleaner.release_key(s3key.name)
            log.warn("Failed to copy file: %s", s3key.name)

    log.info("Finished: %s",batchId)

except Exception as e:
  log.error("Failed to Copy Data", exc_info=True)
  sys.exit()
