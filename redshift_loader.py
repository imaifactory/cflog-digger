#!/usr/bin/env python

import psycopg2
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import logging
import sys
import json
import re

FORMAT = '%(asctime)s %(message)s'
logging.basicConfig(format=FORMAT,level=logging.INFO)
log = logging.getLogger("ex")

try:
    config_path = sys.argv[1]
    config_file = open(config_path)
    config = json.load(config_file)

    __RSCon = config['redshift_host']
    __RSDB =  config['redshift_db']
    __RSPort =config['redshift_port'] 
    __RSU = config['redshift_user']
    __RSPWD = config['redshift_password']
    __RSUserId = 100
    __RSTable = config['redshift_table']

    bn = config['s3_bucket_out_name']
    logprefix = config['s3_bucket_out_logprefix']
    logprefix_for_cleanup = config['s3_bucket_out_logprefix_for_cleanup']
    k_ak = config['s3_bucket_out_ak']
    k_sk = config['s3_bucket_out_sk']

    s3 = S3Connection(k_ak,k_sk)
    bucket = s3.get_bucket(bn)

    re_pattern = 's3://' + bn + '/'

except Exception:
    log.error("Failed load config")
    sys.exit()

def get_conn():
  try: 
    conn = psycopg2.connect(host=__RSCon, database=__RSDB, port=__RSPort, user=__RSU, password=__RSPWD)
    conn.autocommit = True
  except:
    conn = None
  return conn

def exsql(s):
  conn = get_conn()
  
  if conn.closed == 1:
    conn = get_conn()

  cur = conn.cursor()
  cur.execute(s)
  return cur

try:
  s_sql = "copy %s from 's3://{}/{}' credentials 'aws_access_key_id={};aws_secret_access_key={}' IGNOREHEADER 2 GZIP DELIMITER '\t' TRUNCATECOLUMNS BLANKSASNULL FILLRECORD" % __RSTable
  s_sql = s_sql.format(bn,logprefix,k_ak,k_sk)
  exsql(s_sql)
  log.info('Executed Copy Command')
except Exception as e:
  log.error('Failed to Copy Data', exc_info=True)
  sys.exit()

try:
  s_sql = "select top 1 query from STL_QUERY where userid = {} and querytxt like 'copy%' order by starttime desc"
  s_sql = s_sql.format(__RSUserId)
  rs = exsql(s_sql)
  r = rs.fetchone()

  s_sql = "select name from stl_file_scan where query = {} and name like '%.gz%'"
  s_sql = s_sql.format(r[0])
  exsql(s_sql)
  cur = exsql(s_sql)
  rs = cur.fetchall()
  keynames = []
  for file_url in rs:
    keyname = re.sub(re_pattern,'',file_url[0])
    keyname = keyname.rstrip()
    keynames.append(keyname)
    log.info("Loaded to Redshift: %s", keyname)
  log.info('Returned list of files loaded')
except Exception as e:
  log.error('Failed to return list of files loaded', exc_info=True)
  sys.exit()

for src_keyname in keynames:
    src_key = bucket.get_key(src_keyname)
    try:
        data = src_key.get_contents_as_string()
        dst_key = Key(bucket=bucket,name=logprefix_for_cleanup + src_key.name)
        dst_key.set_contents_from_string(data)
    except:
        log.error("Failed to copy from src bucket to archive bucket: %s", src_key.name)
        continue

    try:
        bucket.delete_key(src_key.name)
    except:
        log.error("Failed to delete old file: %s", src_key.name)
    else:
        log.info("Cleanup: %s", src_key.name)


